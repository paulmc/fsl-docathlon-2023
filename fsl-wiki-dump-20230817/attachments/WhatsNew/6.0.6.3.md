### fsleyes             	1.5.0     	-->	1.6.1

- Internal changes to avoid having to overwrite built-in matplotlib colour maps.
- Added an outline button to the overlay display toolbar for mask overlays.
- New --no3DInterp / -ni option, which prevents interpolation from being enabled for volume overlays when a 3D view is opened.
- FSLeyes will now read "default" command-line arguments from a file called default_arguments.txt, stored in the FSLeyes settings directory.
- New --numSlices command-line option for use with lightbox views, which  is an inverted alias for the --sliceSpacing option.
- DICOM directories are now only scanned once, instead of each time they are opened via the Add from DICOM menu option.
- The --useNegativeCmap / -un option is now automatically enabled when --negativeCmap / -nc is specified.
- Fixed some issues related to shutting down FSLeyes cleanly, which could occasionally result in segmentation faults.
- Fixed an issue with tractogram overlays not being drawn in some circumstances.
- Fixed an issue with loading mesh vertex data.
- Fixed an issue with loading annotations when using fsleyes render.


### fslpy               	3.10.0    	-->	3.11.2

- Fixed a bug in the dcm2niix function.
- Adjusted the imrm and imglob scripts to support incomplete wildcard patterns (e.g. img_?? matching img_01.nii.gz).
- New featquery wrapper function.
- fixed the fslstats wrapper to handle index masks (the -K option) with missing label values.
- New standard_space_roi, fslswapdim, fslmerge, fslsplit, fslselectvols, and fslcpgeom wrapper functions.
- New fsl.wrappers.first wrapper functions.
- New fsl.wrappers.bianca and fsl.wrappers.avwutils wrapper functions.
- New fsl.wrappers.bedpostx and fsl.wrappers.dtifit wrapper functions.
- New fsl.wrappers.feat.feat, melodic.fsl_regfilt and melodic.fsl_glm wrapper functions.
- New oxford_asl and asl_file wrapper functions.
- New wrapperconfig context manager function, which allows the default values for arguments passed by the fsl.wrappers functions to the fsl.utils.run.run function to be changed.
- New affine.mergeBounds function.
- The fsl.wrappers.fslmaths.fslmaths and fsl.wrappers.fslstats.fslstats wrapper functions have been updated to accept arguments destined for fsl.utils.run.run.
- Mesh objects can now be created without indices/triangles - they can be assigned after creation.
- The fsl.data.dicom module will now preferentially call $FSLDIR/bin/dcm2niix, instead of calling the first dcm2niix on the $PATH.
- The applyArgStyle argmap argument can now be a callable which defines a rule which will be applied to all argument names.
- The applyArgStyle valmap argument now accepts a new EXPAND_LIST option, which allows sequences to be expanded as separate command-line options.
- Image objects can now be created without passing a nibabel.Nifti1Image (or similar) object, as long as a nibabel.Nifti1Header and a DataManager are provided.
- Fixed a bug in the Image.strval method.


### fsl-add_module      	0.3.2     	-->	0.4.0

- Changed the default behaviour so that only files in the fsl_course_data category are downloaded. This can be overridden by specifying --category all.
- Prefix downloaded files with their category name to reduce the likelihood of collisions across different categories (e.g. registration.tar.gz from the FSL course and from grad course data sets). Nothing is done to prevent collisions in extracted paths from different archives though.
- The --force option can now be used without specifying any modules - it causes all modules in the default/selected cateogories to be downloaded.


### fsl-base            	2212.0    	-->	2302.2

- Small adjustments to the createFSLWrapper script.
- Updated $FSLDIR/etc/fslconf/fsl.csh to bring it in sync with $FSLDIR/etc/fslconf/fsl.sh
- New fslversion command, which prints information about a FSL installation.
- Changed $FSLDIR/bin/Runtcl so that it does not check for a $DISPLAY environment variable (as this variable may not be present on macOS).
- The $CXXFLAGS environment variable is now propagated through to nvcc calls.
- The $FSLDEVDIR destination will be created if it doesn't already exist, when make install is run inside a FSL project directory.
- The update_fsl_package command will add the FSL conda development channel to $FSLDIR/.condarc if the --development option is used.
- The find_cuda_exe command will return the oldest available executable if nvidia-smi reports a CUDA version older than any available executable.


### fsl-bet2            	2111.0    	-->	2111.2

- Add guards for out-of-bounds -f and -g parameters.
- Replace dc calls with syntax that will work on all platforms ("dc -" is unsupported on new versions of macOS).


### fsl-eddy_qc         	v1.1.1    	-->	v1.2.1

- Minor improvements to plots and layout.


### fsl-fabber_models_asl	v2.0.7    	-->	v2.0.8

- Add newinstance calls to work around factory registration broken in build against FSL6.0.6
- Add multiple TEs per TI, ITT inference and separated T2 / T2b inference
- Updates to support compiling against old and new (dynamic linking) versions of FSL


### fsl-fast4           	2111.0    	-->	2111.3

- Fixed and refactored 2-class PVE calculations
- Replace dc calls with syntax that will work on all platforms ("dc -" is unsupported on new macOS)


### fsl-fdt             	2202.5    	-->	2202.6

- replace dc calls with syntax that will work on all platforms ("dc -" is unsupported on new macOS)


### fsl-feat5           	2201.2    	-->	2201.4

- Adjust pnm_stage1 web server creation to avoid pickle problems on macOS.
- Fix file type for slice order file GUI option
- Replace dc calls with syntax that will work on all platforms ("dc -" is unsupported on new macOS)


### fsl-flirt           	2111.0    	-->	2111.1

- Replace dc calls with syntax that will work on all platforms ("dc -" is unsupported on new macOS)


### fsl-installer       	3.0.0     	-->	3.3.0

- Update the installer to install macOS-M1 FSL builds if available.
- Exit with a warning if an Intel FSL build is to be installed on a M1 machine, and Rosetta emulation is not enabled.
- Unrecognised command-line arguments are ignored - this is to allow for forward-compatibility within a self-update cycle.
- bash is used rather than sh when calling the miniconda installer script.
- New hidden --miniconda option, allowing an alternate miniconda installer   to be used.
- Allow different progress reporting implementations
- Clear all $PYTHON* environment variables before installing miniconda and FSL.


### fsl-melodic         	2111.1    	-->	2111.3

- Replace dc calls with syntax that will work on all platforms ("dc -" is unsupported on new macOS)
- Example dual_regression call does not match actual argument parsing


### fsl-miscvis         	2201.0    	-->	2201.1

- Replace dc calls with syntax that will work on all platforms ("dc -" is unsupported on new macOS)


### fsl_mrs             	2.0.9     	-->	2.1.0

- Fix bugs in fmrs stats contrasts.
- Fix fmrs stats error for 1d matrices.
- Move to nifti-mrs dependency.


### fsl-oxford_asl      	v4.0.28   	-->	v4.0.29

- Ignore python build dir
- Remove --save-native-masks option as this isn't suppported in oxford_asl version of ROI stats
- Nifti1Image.get_data raises an error in nibabel 5
- Install python components to $FSLDEVDIR. Remove unused/obsolete targets from SCRIPTS
- Mis-organised kwargs meant that console_script entry points were not being installed
- Set initial choice selection after call to pack, as SetFont seems to reset it on macOS/wxpython 4.1.1


### fsl-pyfeeds         	0.10.1    	-->	0.11.0


- Environment variables and tildes (~) can now be used in the pyfeeds configuration file, for options which expect a file path.
- The pyfeeds hash file will now contain test names, purely for descriptive purposes.


### fsl-pyfeeds-tests   	2210.0    	-->	2302.0

- New pyfeeds test for old feeds tests. Relies on "old_feeds" external data
- Fixes to a few feedsRun scripts


### fsl-randomise       	2203.2    	-->	2203.3

- Replace dc calls with syntax that will work on all platforms ("dc -" is unsupported on new macOS)


### fsl-siena           	2111.0    	-->	2111.1

- replace dc calls with syntax that will work on all platforms ("dc -" is unsupported on new macOS)


### fsl_sub_plugin_slurm	1.4.3     	-->	1.4.4

- os.rename does not always work when tmpdir and cwd are on different file systems. shutil.move is able to handle this with a copy/delete mechanism.
- Use warnings not logger.warn
- Ensure fsl_sub 2.7 is installed


### fsl-tbss            	2111.0    	-->	2111.1

- Replace dc calls with syntax that will work on all platforms ("dc -" is unsupported on new macOS)


### fsl-xtract          	1.5.1     	-->	1.6.2

- Removed xtract_qc install as per SW request
- Baby xtract, fsl_sub updates, xtract_qc, plus minor fixes


### fsl-xtract_data     	1.3.4     	-->	2.0.1

- Baby xtract
